import Vuex from 'vuex';

import cats     from './modules/cats';
import breeds   from './modules/breeds';



export default new Vuex.Store({
    modules: {
        cats,
        breeds
    }
});
