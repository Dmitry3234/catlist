import axios from 'axios';

const state = () =>({
        cats: null,
        cat: null
});

const getters = {
    CATS: (state) => {
        return state.cats;
    },
    CAT: (state) => {
        return state.cat;
    }
};

const mutations = {
    SET_CATS: (state, payload) => {
        state.cats = payload;
    },
    SET_CAT: (state, payload) => {
        state.cat = payload;
    }
};

const actions = {
    GET_CATS: async function(context, payload) {
        let {data} = await axios.get('/api/cats?page=' + payload);
        context.commit("SET_CATS", data.data);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
