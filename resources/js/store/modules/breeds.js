import axios from "axios";

const state = () =>({
        breed: {},
        breeds: {}
});

const getters = {
    BREEDS: (state) => {
        return state.breeds;
    },
    BREED: (state) => {
        return state.breed;
    }
};

const mutations = {
    SET_BREEDS: (state, payload) => {
        state.breeds = payload;
    },
    SET_BREED: (state, payload) => {
        state.breed = payload;
    }
};

const actions = {
    GET_BREEDS: async function(context, payload) {
        let {data} = await axios.get('/api/breeds?page=' + payload);
        context.commit("SET_BREEDS", data.data);
    },

    GET_BREED_FROM_CAT_ID: async function(context, payload) {
        let {data} = await axios.get('/api/cat/' + payload + '/breed');

        context.commit("SET_BREED", data);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
