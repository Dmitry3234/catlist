import { createWebHistory, createRouter } from "vue-router";
import Cats from "./pages/Cats.vue";
import Breeds from "./pages/Breeds.vue";
import Cat from './pages/Cat.vue';

const routes = [
    {
        path: "/cats",
        name: "Cats table",
        component: Cats,
    },
    {
        path: "/breeds",
        name: "Breeds table",
        component: Breeds,
    },
    {
        path: "/cat",
        name: "Кошка)",
        component: Cat
    },
    {
        path: "/:pathMatch(.*)*",
        name: "Cats table",
        component: Cats
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    document.title = to.name;
    next();
});

export default router;
