<?php

/*
 * Main client routes don't require authentication
 */

Route::get('/cats', 'User\CatController@allCats');
Route::get('/cat/{id}/breed', 'User\BreedController@getBreedByCatId');

Route::get('/breeds', 'User\BreedController@getBreeds');
Route::get('/breed/{id}', 'User\BreedController@getBreed');
