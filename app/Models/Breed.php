<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    use HasFactory;

    protected $table = 'breeds';

    protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'description',
        'life_expectancy'
    ];

    public function cats()
    {
        return $this->belongsToMany(Cat::class);
    }
}
