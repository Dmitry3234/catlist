<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BreedController extends Controller
{
    public function getBreeds ()
    {
        $breeds = \App\Models\Breed::paginate(8);

        return $breeds;
    }

    public function getBreed (int $id)
    {
        $breed = \App\Models\Breed::findOrFail($id);

        return $breed;
    }

    public function getBreedByCatId (int $id)
    {
        $breed = \App\Models\Breed::whereHas('cats', function ($q) use ($id)
        {
            $q->where('cat_id', '=', $id);
        })->get();


        return $breed;
    }
}
