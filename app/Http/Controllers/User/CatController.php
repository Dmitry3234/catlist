<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CatController extends Controller
{
    public function allCats (Request $request)
    {
        $pageNum = $request->input('page');
        $cats = \App\Models\Cat::paginate(8);

        return $cats;
    }

}
