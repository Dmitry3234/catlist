<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breed_cat', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('breed_id');
            $table->unsignedBigInteger('cat_id');
            $table->foreign('breed_id')->references('id')->on('breeds');
            $table->foreign('cat_id')->references('id')->on('cats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breed_cat');
    }
};
