<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CatBreedRelationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $breeds = \App\Models\Breed::all();

       if ($breeds)
       {
           \App\Models\Cat::all()->each(function ($cat) use ($breeds) {
               $cat->breeds()->attach(
                   $breeds->random(1)->pluck('id')->toArray()
               );
           });
       }
    }
}
